import { sixteenBitAdd } from "../bits";
import { CPU, IInstruction, IReadable, IReadWritable } from "../cpu/cpu";

export default class Add16Instruction implements IInstruction {
  constructor(
    readonly cpu: CPU,
    readonly to: IReadWritable,
    readonly from: IReadable,
    readonly cycles: number,
  ) {}

  public execute() {
    const a = this.to.read();
    const b = this.from.read();

    const { sum, carry, halfCarry } = sixteenBitAdd(a, b);

    this.to.write(sum);
    this.cpu.F.setFlags({ carry, halfCarry, subtract: false });
  }
}
