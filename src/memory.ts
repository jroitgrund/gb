export interface IMemory {
  read(address: number): number;
  read16(address: number): number;
  write(address: number, data: number): void;
  write16(address: number, data: number): void;
}

export class Memory implements IMemory {
  public read(address: number): number {
    return address;
  }

  public read16(address: number): number {
    return address;
  }

  public write(address: number, data: number) {
    console.log(address);
    console.log(data);
  }

  public write16(address: number, data: number) {
    console.log(address);
    console.log(data);
  }
}
