import ReadAndIncrement from "../ReadAndIncrement";
import Register from "../Register";

test("ReadAndIncrement", () => {
  const register = new Register();
  register.write(5);

  const incrementing = new ReadAndIncrement(register);
  expect(incrementing.read()).toEqual(5);
  expect(incrementing.read()).toEqual(6);
});
