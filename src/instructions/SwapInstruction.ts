import { CPU, IInstruction, IReadWritable } from "../cpu/cpu";

export default class DecrementInstruction implements IInstruction {
  constructor(
    readonly cpu: CPU,
    readonly to: IReadWritable,
    readonly cycles: number,
  ) {}

  public execute() {
    const a = this.to.read();

    const lowerHighered = (a & 0xf) << 4;
    const higherLowered = a >> 4;

    this.to.write(higherLowered | lowerHighered);
    this.cpu.F.setFlags({
      zero: a === 0,
      halfCarry: false,
      carry: false,
      subtract: false,
    });
  }
}
