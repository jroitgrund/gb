export function getMSB(data: number) {
  return data >> 8;
}

export function getLSB(data: number) {
  return data & 0xff;
}

export function getTwoBytes(LSB: number, MSB: number) {
  return (MSB << 8) | LSB;
}

export function sixteenBitAdd(a: number, b: number) {
  const eightBitSum = eightBitAdd(a & 0xff, b & 0xff);

  const totalSum = eightBitAdd(
    a & 0xff00,
    b & 0xff00,
    eightBitSum.carry ? 1 : 0,
  );

  const sum = totalSum.sum & 0xffff;
  const halfCarry = eightBitSum.carry;
  const carry = totalSum.sum > 0xffff;

  return {
    sum,
    halfCarry,
    carry,
  };
}

export function eightBitAdd(a: number, b: number, ...more: number[]) {
  const overflowingSum = more.reduce(
    (currSum, currNum) => currSum + currNum,
    a + b,
  );
  const halfCarry =
    more.reduce(
      (currSum, currNum) => currSum + (currNum & 0xf),
      (a & 0xf) + (b & 0xf),
    ) > 0xf;

  const carry = overflowingSum > 0xff;
  const sum = overflowingSum & 0xff;
  const zero = sum === 0;

  return {
    sum,
    carry,
    halfCarry,
    zero,
  };
}

export function eightBitSub(a: number, b: number, ...more: number[]) {
  const minusB = (~b + 1) & 0xff;
  const minusMore = more.map(n => (~n + 1) & 0xff);
  const { sum, ...flags } = eightBitAdd(a, minusB, ...minusMore);
  return {
    ...flags,
    difference: sum,
  };
}

export function eightBitSigned(n: number) {
  const highBit = (n & (1 << 7)) === 1 << 7;
  const unsigned = n & ~(1 << 7);
  return (highBit ? -128 : 0) + unsigned;
}

export function isBitSet(data: number, bit: number) {
  return (data & (1 << bit)) > 0;
}
