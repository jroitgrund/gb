import ReadAndDecrement from "../ReadAndDecrement";
import Register from "../Register";

test("ReadAndDecrement", () => {
  const register = new Register();
  register.write(5);

  const decrementing = new ReadAndDecrement(register);
  expect(decrementing.read()).toEqual(5);
  expect(decrementing.read()).toEqual(4);
});
