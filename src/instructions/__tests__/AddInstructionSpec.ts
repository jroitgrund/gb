import { CPU } from "../../cpu/cpu";
import { ICurrentFlags } from "../../readers-writers/Flags";
import AddInstruction, { withCarry } from "../AddInstruction";

let cpu: CPU;

beforeEach(() => {
  cpu = new CPU();
  cpu.F.setFlags({ carry: true });
});

function generateTest(
  name: string,
  a: number,
  b: number,
  flags: ICurrentFlags,
  sum: number,
  carry: boolean = false,
) {
  test(name, () => {
    cpu.A.write(a);
    cpu.B.write(b);
    new AddInstruction(
      cpu,
      cpu.A,
      cpu.B,
      4,
      carry ? withCarry : undefined,
    ).execute();

    expect(cpu.F.getFlags()).toEqual(flags);

    expect(cpu.A.read()).toEqual(sum);
    expect(cpu.B.read()).toEqual(b);
  });
}

[
  [
    "Simple addition",
    4,
    5,
    {
      zero: false,
      carry: false,
      halfCarry: false,
      subtract: false,
    },
    9,
  ],
  [
    "Half carry",
    8,
    9,
    {
      zero: false,
      carry: false,
      halfCarry: true,
      subtract: false,
    },
    17,
  ],
  [
    "Carry and zero",
    255,
    1,
    {
      zero: true,
      carry: true,
      halfCarry: true,
      subtract: false,
    },
    0,
  ],
  [
    "With existing carry",
    255,
    0,
    {
      zero: true,
      carry: true,
      halfCarry: true,
      subtract: false,
    },
    0,
    true,
  ],
].forEach(row => {
  const [name, a, b, flags, sum, carry] = row;
  generateTest(
    name as string,
    a as number,
    b as number,
    flags as ICurrentFlags,
    sum as number,
    carry as boolean,
  );
});
