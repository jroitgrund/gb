import { CPU, IReadable } from "../cpu/cpu";
import { IMemory } from "../memory";

export default class SixteenBitLiteral implements IReadable {
  constructor(readonly cpu: CPU, readonly memory: IMemory) {}

  public read() {
    const pc = this.cpu.PC.read();
    this.cpu.PC.write(pc + 2);
    return this.memory.read16(pc);
  }
}
