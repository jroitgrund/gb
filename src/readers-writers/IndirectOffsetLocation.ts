import { IReadable, IReadWritable } from "../cpu/cpu";
import { IMemory } from "../memory";

export default class IndirectOffsetLocation implements IReadWritable {
  private static OFFSET_FROM: number = 0xff00;

  constructor(readonly memory: IMemory, readonly location: IReadable) {}

  public read() {
    return this.memory.read(
      IndirectOffsetLocation.OFFSET_FROM + this.location.read(),
    );
  }

  public write(data: number) {
    return this.memory.write(
      IndirectOffsetLocation.OFFSET_FROM + this.location.read(),
      data,
    );
  }
}
