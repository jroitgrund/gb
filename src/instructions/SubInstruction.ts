import { eightBitSub } from "../bits";
import { CPU, IInstruction, IReadable, IReadWritable } from "../cpu/cpu";

export default class SubInstruction implements IInstruction {
  constructor(
    readonly cpu: CPU,
    readonly to: IReadWritable,
    readonly from: IReadable,
    readonly cycles: number,
    readonly carrySupplier: (cpu: CPU) => number = () => 0,
  ) {}

  public execute() {
    const a = this.to.read();
    const b = this.from.read();

    const { difference, carry, halfCarry, zero } = eightBitSub(
      a,
      b,
      this.carrySupplier(this.cpu),
    );

    this.to.write(difference);
    this.cpu.F.setFlags({ carry, halfCarry, zero, subtract: true });
  }
}
