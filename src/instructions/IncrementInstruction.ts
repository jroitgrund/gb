import { eightBitAdd } from "../bits";
import { CPU, IInstruction, IReadWritable } from "../cpu/cpu";

export default class IncrementInstruction implements IInstruction {
  constructor(
    readonly cpu: CPU,
    readonly to: IReadWritable,
    readonly cycles: number,
  ) {}

  public execute() {
    const a = this.to.read();

    const { sum, halfCarry, zero } = eightBitAdd(a, 1);

    this.to.write(sum);
    this.cpu.F.setFlags({ halfCarry, zero, subtract: false });
  }
}
