import { getLSB, getMSB, getTwoBytes } from "../bits";
import { IMemory } from "../memory";

export default class FakeMemory implements IMemory {
  private memory = new Uint8Array(0xffff);

  public read(address: number) {
    return this.memory[address];
  }

  public read16(address: number) {
    return getTwoBytes(this.memory[address], this.memory[address + 1]);
  }

  public write(address: number, data: number) {
    this.memory[address] = data;
  }

  public write16(address: number, data: number) {
    this.memory[address] = getLSB(data);
    this.memory[address + 1] = getMSB(data);
  }
}
