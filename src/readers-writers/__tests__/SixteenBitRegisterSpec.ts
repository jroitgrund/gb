import Register from "../Register";
import SixteenBitRegister from "../SixteenBitRegister";

test("ReadAndIncrement", () => {
  const register = new SixteenBitRegister(new Register(), new Register());
  register.write(0xabcd);

  expect(register.read()).toEqual(0xabcd);
});
