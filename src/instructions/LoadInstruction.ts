import { IInstruction, IReadable, IWritable } from "../cpu/cpu";

export default class LoadInstruction implements IInstruction {
  constructor(
    readonly to: IWritable,
    readonly from: IReadable,
    readonly cycles: number,
  ) {}

  public execute() {
    this.to.write(this.from.read());
  }
}
