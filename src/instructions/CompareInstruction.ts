import { eightBitSub } from "../bits";
import { CPU, IInstruction, IReadable, IReadWritable } from "../cpu/cpu";

export default class CompareInstruction implements IInstruction {
  constructor(
    readonly cpu: CPU,
    readonly to: IReadWritable,
    readonly from: IReadable,
    readonly cycles: number,
  ) {}

  public execute() {
    const a = this.to.read();
    const b = this.from.read();

    const { carry, halfCarry, zero } = eightBitSub(a, b);

    this.cpu.F.setFlags({ carry, halfCarry, zero, subtract: true });
  }
}
