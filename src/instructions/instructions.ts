import { CPU } from "../cpu/cpu";
import { IMemory } from "../memory";
import EightBitLiteral from "../readers-writers/EightBitLiteral";
import IndirectMemoryLocation from "../readers-writers/IndirectMemoryLocation";
import IndirectOffsetLocation from "../readers-writers/IndirectOffsetLocation";
import ReadAndDecrement from "../readers-writers/ReadAndDecrement";
import ReadAndIncrement from "../readers-writers/ReadAndIncrement";
import SixteenBitLiteral from "../readers-writers/SixteenBitLiteral";
import StackPointerPlusEightBitLiteral from "../readers-writers/StackPointerPlusEightBitLiteral";

import Add16Instruction from "./AddInstruction";
import AddInstruction, { withCarry } from "./AddInstruction";
import CompareInstruction from "./CompareInstruction";
import Decrement16Instruction from "./Decrement16Instruction";
import DecrementInstruction from "./DecrementInstruction";
import Increment16Instruction from "./Increment16Instruction";
import IncrementInstruction from "./IncrementInstruction";
import LoadInstruction from "./LoadInstruction";
import OrInstruction from "./OrInstruction";
import AndInstruction from "./OrInstruction";
import PopInstruction from "./PopInstruction";
import PushInstruction from "./PushInstruction";
import SubInstruction from "./SubInstruction";
import XorInstruction from "./XorInstruction";

export function getInstructions(cpu: CPU, memory: IMemory) {
  const eightBitLiteral = new EightBitLiteral(cpu, memory);
  const sixteenBitLiteral = new SixteenBitLiteral(cpu, memory);

  const literalMemory = new IndirectMemoryLocation(memory, sixteenBitLiteral);

  const HLAndDecrement = new ReadAndDecrement(cpu.HL);
  const HLAndIncrement = new ReadAndIncrement(cpu.HL);
  const HLMemory = new IndirectMemoryLocation(memory, cpu.HL);
  const HLAndDecrementMemory = new IndirectMemoryLocation(
    memory,
    HLAndDecrement,
  );
  const HLAndIncrementMemory = new IndirectMemoryLocation(
    memory,
    HLAndIncrement,
  );

  const BCMemory = new IndirectMemoryLocation(memory, cpu.BC);

  const DEMemory = new IndirectMemoryLocation(memory, cpu.DE);

  const cOffsetMemory = new IndirectOffsetLocation(memory, cpu.C);
  const literalOffsetMemory = new IndirectOffsetLocation(
    memory,
    eightBitLiteral,
  );

  const stackPointerPlusEightBitLiteral = new StackPointerPlusEightBitLiteral(
    cpu,
    eightBitLiteral,
  );

  return {
    [0x06]: new LoadInstruction(cpu.B, eightBitLiteral, 8),
    [0x0e]: new LoadInstruction(cpu.C, eightBitLiteral, 8),
    [0x16]: new LoadInstruction(cpu.D, eightBitLiteral, 8),
    [0x1e]: new LoadInstruction(cpu.E, eightBitLiteral, 8),
    [0x26]: new LoadInstruction(cpu.H, eightBitLiteral, 8),
    [0x2e]: new LoadInstruction(cpu.L, eightBitLiteral, 8),

    [0x7f]: new LoadInstruction(cpu.A, cpu.A, 4),
    [0x78]: new LoadInstruction(cpu.A, cpu.B, 4),
    [0x79]: new LoadInstruction(cpu.A, cpu.C, 4),
    [0x7a]: new LoadInstruction(cpu.A, cpu.D, 4),
    [0x7b]: new LoadInstruction(cpu.A, cpu.E, 4),
    [0x7c]: new LoadInstruction(cpu.A, cpu.H, 4),
    [0x7d]: new LoadInstruction(cpu.A, cpu.L, 4),
    [0x7e]: new LoadInstruction(cpu.A, HLMemory, 8),

    [0x40]: new LoadInstruction(cpu.B, cpu.B, 4),
    [0x41]: new LoadInstruction(cpu.B, cpu.C, 4),
    [0x42]: new LoadInstruction(cpu.B, cpu.D, 4),
    [0x43]: new LoadInstruction(cpu.B, cpu.E, 4),
    [0x44]: new LoadInstruction(cpu.B, cpu.H, 4),
    [0x45]: new LoadInstruction(cpu.B, cpu.L, 4),
    [0x46]: new LoadInstruction(cpu.B, HLMemory, 8),

    [0x48]: new LoadInstruction(cpu.C, cpu.B, 4),
    [0x49]: new LoadInstruction(cpu.C, cpu.C, 4),
    [0x4a]: new LoadInstruction(cpu.C, cpu.D, 4),
    [0x4b]: new LoadInstruction(cpu.C, cpu.E, 4),
    [0x4c]: new LoadInstruction(cpu.C, cpu.H, 4),
    [0x4d]: new LoadInstruction(cpu.C, cpu.L, 4),
    [0x4e]: new LoadInstruction(cpu.C, HLMemory, 8),

    [0x50]: new LoadInstruction(cpu.D, cpu.B, 4),
    [0x51]: new LoadInstruction(cpu.D, cpu.C, 4),
    [0x52]: new LoadInstruction(cpu.D, cpu.D, 4),
    [0x53]: new LoadInstruction(cpu.D, cpu.E, 4),
    [0x54]: new LoadInstruction(cpu.D, cpu.H, 4),
    [0x55]: new LoadInstruction(cpu.D, cpu.L, 4),
    [0x56]: new LoadInstruction(cpu.D, HLMemory, 8),

    [0x58]: new LoadInstruction(cpu.E, cpu.B, 4),
    [0x59]: new LoadInstruction(cpu.E, cpu.C, 4),
    [0x5a]: new LoadInstruction(cpu.E, cpu.D, 4),
    [0x5b]: new LoadInstruction(cpu.E, cpu.E, 4),
    [0x5c]: new LoadInstruction(cpu.E, cpu.H, 4),
    [0x5d]: new LoadInstruction(cpu.E, cpu.L, 4),
    [0x5e]: new LoadInstruction(cpu.E, HLMemory, 8),

    [0x60]: new LoadInstruction(cpu.H, cpu.B, 4),
    [0x61]: new LoadInstruction(cpu.H, cpu.C, 4),
    [0x62]: new LoadInstruction(cpu.H, cpu.D, 4),
    [0x63]: new LoadInstruction(cpu.H, cpu.E, 4),
    [0x64]: new LoadInstruction(cpu.H, cpu.H, 4),
    [0x65]: new LoadInstruction(cpu.H, cpu.L, 4),
    [0x66]: new LoadInstruction(cpu.H, HLMemory, 8),

    [0x68]: new LoadInstruction(cpu.L, cpu.B, 4),
    [0x69]: new LoadInstruction(cpu.L, cpu.C, 4),
    [0x6a]: new LoadInstruction(cpu.L, cpu.D, 4),
    [0x6b]: new LoadInstruction(cpu.L, cpu.E, 4),
    [0x6c]: new LoadInstruction(cpu.L, cpu.H, 4),
    [0x6d]: new LoadInstruction(cpu.L, cpu.L, 4),
    [0x6e]: new LoadInstruction(cpu.L, HLMemory, 8),

    [0x70]: new LoadInstruction(HLMemory, cpu.B, 8),
    [0x71]: new LoadInstruction(HLMemory, cpu.C, 8),
    [0x72]: new LoadInstruction(HLMemory, cpu.D, 8),
    [0x73]: new LoadInstruction(HLMemory, cpu.E, 8),
    [0x74]: new LoadInstruction(HLMemory, cpu.H, 8),
    [0x75]: new LoadInstruction(HLMemory, cpu.L, 8),
    [0x36]: new LoadInstruction(HLMemory, eightBitLiteral, 12),

    [0x0a]: new LoadInstruction(cpu.A, BCMemory, 8),
    [0x1a]: new LoadInstruction(cpu.A, DEMemory, 8),
    [0xfa]: new LoadInstruction(cpu.A, literalMemory, 16),
    [0x3e]: new LoadInstruction(cpu.A, eightBitLiteral, 8),

    [0x47]: new LoadInstruction(cpu.B, cpu.A, 4),
    [0x4f]: new LoadInstruction(cpu.C, cpu.A, 4),
    [0x57]: new LoadInstruction(cpu.D, cpu.A, 4),
    [0x5f]: new LoadInstruction(cpu.E, cpu.A, 4),
    [0x67]: new LoadInstruction(cpu.H, cpu.A, 4),
    [0x6f]: new LoadInstruction(cpu.L, cpu.A, 4),
    [0x02]: new LoadInstruction(BCMemory, cpu.A, 8),
    [0x12]: new LoadInstruction(DEMemory, cpu.A, 8),
    [0x77]: new LoadInstruction(HLMemory, cpu.A, 8),
    [0xea]: new LoadInstruction(literalMemory, cpu.A, 16),

    [0xf2]: new LoadInstruction(cpu.A, cOffsetMemory, 8),
    [0xe2]: new LoadInstruction(cOffsetMemory, cpu.A, 8),

    [0x3a]: new LoadInstruction(cpu.A, HLAndDecrementMemory, 8),
    [0x32]: new LoadInstruction(HLAndDecrementMemory, cpu.A, 8),
    [0x2a]: new LoadInstruction(cpu.A, HLAndIncrementMemory, 8),
    [0x22]: new LoadInstruction(HLAndIncrementMemory, cpu.A, 8),

    [0xe0]: new LoadInstruction(literalOffsetMemory, cpu.A, 12),
    [0xf0]: new LoadInstruction(cpu.A, literalOffsetMemory, 12),

    [0x01]: new LoadInstruction(cpu.BC, sixteenBitLiteral, 12),
    [0x11]: new LoadInstruction(cpu.DE, sixteenBitLiteral, 12),
    [0x21]: new LoadInstruction(cpu.HL, sixteenBitLiteral, 12),
    [0x31]: new LoadInstruction(cpu.SP, sixteenBitLiteral, 12),

    [0xf9]: new LoadInstruction(cpu.SP, cpu.HL, 8),

    [0xf8]: new LoadInstruction(cpu.HL, stackPointerPlusEightBitLiteral, 12),

    [0x08]: new LoadInstruction(literalMemory, cpu.SP, 20),

    [0xf5]: new PushInstruction(cpu, memory, cpu.AF, 16),
    [0xc5]: new PushInstruction(cpu, memory, cpu.BC, 16),
    [0xd5]: new PushInstruction(cpu, memory, cpu.DE, 16),
    [0xe5]: new PushInstruction(cpu, memory, cpu.HL, 16),

    [0xf1]: new PopInstruction(cpu, memory, cpu.AF, 12),
    [0xc1]: new PopInstruction(cpu, memory, cpu.BC, 12),
    [0xd1]: new PopInstruction(cpu, memory, cpu.DE, 12),
    [0xe1]: new PopInstruction(cpu, memory, cpu.HL, 12),

    [0x87]: new AddInstruction(cpu, cpu.A, cpu.A, 4),
    [0x80]: new AddInstruction(cpu, cpu.A, cpu.B, 4),
    [0x81]: new AddInstruction(cpu, cpu.A, cpu.C, 4),
    [0x82]: new AddInstruction(cpu, cpu.A, cpu.D, 4),
    [0x83]: new AddInstruction(cpu, cpu.A, cpu.E, 4),
    [0x84]: new AddInstruction(cpu, cpu.A, cpu.H, 4),
    [0x85]: new AddInstruction(cpu, cpu.A, cpu.L, 4),
    [0x86]: new AddInstruction(cpu, cpu.A, HLMemory, 8),
    [0xc6]: new AddInstruction(cpu, cpu.A, eightBitLiteral, 8),

    [0x8f]: new AddInstruction(cpu, cpu.A, cpu.A, 4, withCarry),
    [0x88]: new AddInstruction(cpu, cpu.A, cpu.B, 4, withCarry),
    [0x89]: new AddInstruction(cpu, cpu.A, cpu.C, 4, withCarry),
    [0x8a]: new AddInstruction(cpu, cpu.A, cpu.D, 4, withCarry),
    [0x8b]: new AddInstruction(cpu, cpu.A, cpu.E, 4, withCarry),
    [0x8c]: new AddInstruction(cpu, cpu.A, cpu.H, 4, withCarry),
    [0x8d]: new AddInstruction(cpu, cpu.A, cpu.L, 4, withCarry),
    [0x8e]: new AddInstruction(cpu, cpu.A, HLMemory, 8, withCarry),
    [0xce]: new AddInstruction(cpu, cpu.A, eightBitLiteral, 8, withCarry),

    [0x97]: new SubInstruction(cpu, cpu.A, cpu.A, 4),
    [0x90]: new SubInstruction(cpu, cpu.A, cpu.B, 4),
    [0x91]: new SubInstruction(cpu, cpu.A, cpu.C, 4),
    [0x92]: new SubInstruction(cpu, cpu.A, cpu.D, 4),
    [0x93]: new SubInstruction(cpu, cpu.A, cpu.E, 4),
    [0x94]: new SubInstruction(cpu, cpu.A, cpu.H, 4),
    [0x95]: new SubInstruction(cpu, cpu.A, cpu.L, 4),
    [0x96]: new SubInstruction(cpu, cpu.A, HLMemory, 8),
    [0xd6]: new SubInstruction(cpu, cpu.A, eightBitLiteral, 8),

    [0x9f]: new SubInstruction(cpu, cpu.A, cpu.A, 4, withCarry),
    [0x98]: new SubInstruction(cpu, cpu.A, cpu.B, 4, withCarry),
    [0x99]: new SubInstruction(cpu, cpu.A, cpu.C, 4, withCarry),
    [0x9a]: new SubInstruction(cpu, cpu.A, cpu.D, 4, withCarry),
    [0x9b]: new SubInstruction(cpu, cpu.A, cpu.E, 4, withCarry),
    [0x9c]: new SubInstruction(cpu, cpu.A, cpu.H, 4, withCarry),
    [0x9d]: new SubInstruction(cpu, cpu.A, cpu.L, 4, withCarry),
    [0x9e]: new SubInstruction(cpu, cpu.A, HLMemory, 8, withCarry),
    [0xde]: new SubInstruction(cpu, cpu.A, eightBitLiteral, 8, withCarry),

    [0xa7]: new AndInstruction(cpu, cpu.A, cpu.A, 4),
    [0xa0]: new AndInstruction(cpu, cpu.A, cpu.B, 4),
    [0xa1]: new AndInstruction(cpu, cpu.A, cpu.C, 4),
    [0xa2]: new AndInstruction(cpu, cpu.A, cpu.D, 4),
    [0xa3]: new AndInstruction(cpu, cpu.A, cpu.E, 4),
    [0xa4]: new AndInstruction(cpu, cpu.A, cpu.H, 4),
    [0xa5]: new AndInstruction(cpu, cpu.A, cpu.L, 4),
    [0xa6]: new AndInstruction(cpu, cpu.A, HLMemory, 8),
    [0xe6]: new AndInstruction(cpu, cpu.A, eightBitLiteral, 8),

    [0xb7]: new OrInstruction(cpu, cpu.A, cpu.A, 4),
    [0xb0]: new OrInstruction(cpu, cpu.A, cpu.B, 4),
    [0xb1]: new OrInstruction(cpu, cpu.A, cpu.C, 4),
    [0xb2]: new OrInstruction(cpu, cpu.A, cpu.D, 4),
    [0xb3]: new OrInstruction(cpu, cpu.A, cpu.E, 4),
    [0xb4]: new OrInstruction(cpu, cpu.A, cpu.H, 4),
    [0xb5]: new OrInstruction(cpu, cpu.A, cpu.L, 4),
    [0xb6]: new OrInstruction(cpu, cpu.A, HLMemory, 8),
    [0xf6]: new OrInstruction(cpu, cpu.A, eightBitLiteral, 8),

    [0xaf]: new XorInstruction(cpu, cpu.A, cpu.A, 4),
    [0xa8]: new XorInstruction(cpu, cpu.A, cpu.B, 4),
    [0xa9]: new XorInstruction(cpu, cpu.A, cpu.C, 4),
    [0xaa]: new XorInstruction(cpu, cpu.A, cpu.D, 4),
    [0xab]: new XorInstruction(cpu, cpu.A, cpu.E, 4),
    [0xac]: new XorInstruction(cpu, cpu.A, cpu.H, 4),
    [0xad]: new XorInstruction(cpu, cpu.A, cpu.L, 4),
    [0xae]: new XorInstruction(cpu, cpu.A, HLMemory, 8),
    [0xee]: new XorInstruction(cpu, cpu.A, eightBitLiteral, 8),

    [0xbf]: new CompareInstruction(cpu, cpu.A, cpu.A, 4),
    [0xb8]: new CompareInstruction(cpu, cpu.A, cpu.B, 4),
    [0xb9]: new CompareInstruction(cpu, cpu.A, cpu.C, 4),
    [0xba]: new CompareInstruction(cpu, cpu.A, cpu.D, 4),
    [0xbb]: new CompareInstruction(cpu, cpu.A, cpu.E, 4),
    [0xbc]: new CompareInstruction(cpu, cpu.A, cpu.H, 4),
    [0xbd]: new CompareInstruction(cpu, cpu.A, cpu.L, 4),
    [0xbe]: new CompareInstruction(cpu, cpu.A, HLMemory, 8),
    [0xfe]: new CompareInstruction(cpu, cpu.A, eightBitLiteral, 8),

    [0x3c]: new IncrementInstruction(cpu, cpu.A, 4),
    [0x04]: new IncrementInstruction(cpu, cpu.B, 4),
    [0x0c]: new IncrementInstruction(cpu, cpu.C, 4),
    [0x14]: new IncrementInstruction(cpu, cpu.D, 4),
    [0x1c]: new IncrementInstruction(cpu, cpu.E, 4),
    [0x24]: new IncrementInstruction(cpu, cpu.H, 4),
    [0x2c]: new IncrementInstruction(cpu, cpu.L, 4),
    [0x34]: new IncrementInstruction(cpu, HLMemory, 12),

    [0x3d]: new DecrementInstruction(cpu, cpu.A, 4),
    [0x0d]: new DecrementInstruction(cpu, cpu.B, 4),
    [0x05]: new DecrementInstruction(cpu, cpu.C, 4),
    [0x1d]: new DecrementInstruction(cpu, cpu.D, 4),
    [0x15]: new DecrementInstruction(cpu, cpu.E, 4),
    [0x2d]: new DecrementInstruction(cpu, cpu.H, 4),
    [0x25]: new DecrementInstruction(cpu, cpu.L, 4),
    [0x35]: new DecrementInstruction(cpu, HLMemory, 12),

    [0x09]: new Add16Instruction(cpu, cpu.HL, cpu.BC, 8),
    [0x19]: new Add16Instruction(cpu, cpu.HL, cpu.DE, 8),
    [0x29]: new Add16Instruction(cpu, cpu.HL, cpu.HL, 8),
    [0x39]: new Add16Instruction(cpu, cpu.HL, cpu.SP, 8),

    [0xe8]: {
      cycles: 16,
      execute() {
        cpu.SP.write(stackPointerPlusEightBitLiteral.read());
      },
    },

    [0x03]: new Increment16Instruction(cpu, cpu.BC, 8),
    [0x13]: new Increment16Instruction(cpu, cpu.DE, 8),
    [0x23]: new Increment16Instruction(cpu, cpu.HL, 8),
    [0x33]: new Increment16Instruction(cpu, cpu.SP, 8),

    [0x0b]: new Decrement16Instruction(cpu, cpu.BC, 8),
    [0x1b]: new Decrement16Instruction(cpu, cpu.DE, 8),
    [0x2b]: new Decrement16Instruction(cpu, cpu.HL, 8),
    [0x3b]: new Decrement16Instruction(cpu, cpu.SP, 8),
  };
}
