import { CPU, IInstruction, IReadable } from "../cpu/cpu";
import { IMemory } from "../memory";

export default class PushInstruction implements IInstruction {
  constructor(
    readonly cpu: CPU,
    readonly memory: IMemory,
    readonly from: IReadable,
    readonly cycles: number,
  ) {}

  public execute() {
    const data = this.from.read();
    const sp = this.cpu.SP.read() - 2;
    this.memory.write16(sp, data);
    this.cpu.SP.write(sp);
  }
}
