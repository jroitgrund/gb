import {
  eightBitSigned,
  eightBitSub,
  getLSB,
  getMSB,
  getTwoBytes,
  isBitSet,
} from "../bits";

test("getMSB", () => {
  expect(getMSB(0xdcba)).toEqual(0xdc);
  expect(getMSB(0xabcd)).toEqual(0xab);
});

test("getLSB", () => {
  expect(getLSB(0xdcba)).toEqual(0xba);
  expect(getLSB(0xabcd)).toEqual(0xcd);
});

test("getTwoBytes", () => {
  expect(getTwoBytes(0xab, 0xcd)).toEqual(0xcdab);
  expect(getTwoBytes(0xba, 0xdc)).toEqual(0xdcba);
});

test("eightBitSigned", () => {
  expect(eightBitSigned(0b01111111)).toEqual(127);
  expect(eightBitSigned(0b10000000)).toEqual(-128);
  expect(eightBitSigned(0b10000001)).toEqual(-127);
  expect(eightBitSigned(0b00000000)).toEqual(0);
});

test("isBitSet", () => {
  expect(isBitSet(0b1011, 0)).toEqual(true);
  expect(isBitSet(0b1011, 1)).toEqual(true);
  expect(isBitSet(0b1011, 2)).toEqual(false);
  expect(isBitSet(0b1011, 3)).toEqual(true);
});

[[255, 0, 255], [0, 255, 1], [125, 125, 0]].forEach((row, i) => {
  test(`Subtraction test ${i}`, () => {
    const [a, b, result] = row;
    expect(eightBitSub(a, b).difference).toEqual(result);
  });
});
