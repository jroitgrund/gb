import { eightBitAdd, eightBitSigned } from "../bits";
import { CPU, IReadable } from "../cpu/cpu";

import EightBitLiteral from "./EightBitLiteral";

export default class StackPointerPlusEightBitLiteral implements IReadable {
  constructor(readonly cpu: CPU, readonly eightBitLiteral: EightBitLiteral) {}

  public read() {
    const sp = this.cpu.SP.read();
    const n = this.eightBitLiteral.read();
    const { carry, halfCarry } = eightBitAdd(sp, n);

    const sum = sp + eightBitSigned(n);

    this.cpu.F.setFlags({
      zero: false,
      subtract: false,
      halfCarry,
      carry,
    });

    return 0xffff & sum;
  }
}
