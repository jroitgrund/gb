import { eightBitSub } from "../bits";
import { CPU, IInstruction, IReadWritable } from "../cpu/cpu";

export default class DecrementInstruction implements IInstruction {
  constructor(
    readonly cpu: CPU,
    readonly to: IReadWritable,
    readonly cycles: number,
  ) {}

  public execute() {
    const a = this.to.read();

    const { difference, halfCarry, zero } = eightBitSub(a, 1);

    this.to.write(difference);
    this.cpu.F.setFlags({ halfCarry, zero, subtract: true });
  }
}
