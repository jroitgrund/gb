import { CPU } from "../../cpu/cpu";
import FakeMemory from "../../test-util/FakeMemory";
import SixteenBitLiteral from "../SixteenBitLiteral";

const MEMORY_LOCATION = 500;
const VALUE = 0xabcd;

test("SixteenBitLiteral", () => {
  const cpu = new CPU();
  cpu.PC.write(MEMORY_LOCATION);

  const memory = new FakeMemory();
  memory.write16(MEMORY_LOCATION, VALUE);

  const sixteenBitLiteral = new SixteenBitLiteral(cpu, memory);
  expect(sixteenBitLiteral.read()).toEqual(VALUE);
  expect(cpu.PC.read()).toEqual(MEMORY_LOCATION + 2);
});
