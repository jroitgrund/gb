import { CPU, IInstruction, IReadWritable } from "../cpu/cpu";

const MINUS_ONE = (~1 + 1) & 0xff;

export default class Decrement16Instruction implements IInstruction {
  constructor(
    readonly cpu: CPU,
    readonly to: IReadWritable,
    readonly cycles: number,
  ) {}

  public execute() {
    const a = this.to.read();
    this.to.write((a + MINUS_ONE) & 0xffff);
  }
}
