import { mapValues } from "lodash";

import { isBitSet } from "../bits";
import { IReadWritable } from "../cpu/cpu";

export interface IFlagChanges {
  zero?: boolean;
  subtract?: boolean;
  halfCarry?: boolean;
  carry?: boolean;
}

export interface ICurrentFlags {
  zero: boolean;
  subtract: boolean;
  halfCarry: boolean;
  carry: boolean;
}

export class Flags implements IReadWritable {
  private static OFFSETS = {
    zero: 7,
    subtract: 6,
    halfCarry: 5,
    carry: 4,
  };

  private register = 0;

  public getFlags(): ICurrentFlags {
    return mapValues(Flags.OFFSETS, offset =>
      isBitSet(this.register, offset),
    ) as any;
  }

  public setFlags(changes: IFlagChanges) {
    for (const flag of Object.keys(changes)) {
      const truth: boolean = (changes as any)[flag];
      const offset = (Flags.OFFSETS as any)[flag];
      if (truth) {
        this.set(offset);
      } else {
        this.clear(offset);
      }
    }
  }

  public read() {
    return this.register;
  }

  public write(data: number) {
    this.register = data;
  }

  private clear(offset: number) {
    this.register &= 0xff ^ (1 << offset);
  }

  private set(offset: number) {
    this.register |= 1 << offset;
  }
}
