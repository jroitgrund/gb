import { Flags } from "../readers-writers/Flags";
import Register from "../readers-writers/Register";
import SixteenBitRegister from "../readers-writers/SixteenBitRegister";

export interface IInstruction {
  cycles: number;
  execute: () => void;
}

export interface IReadable {
  read: () => number;
}

export interface IWritable {
  write: (data: number) => void;
}

export interface IReadWritable extends IReadable, IWritable {}

export class CPU {
  public A = new Register();
  public B = new Register();
  public C = new Register();
  public D = new Register();
  public E = new Register();
  public H = new Register();
  public L = new Register();
  public F = new Flags();

  public SP = new SixteenBitRegister(new Register(), new Register());
  public PC = new SixteenBitRegister(new Register(), new Register());

  public HL = new SixteenBitRegister(this.H, this.L);
  public AF = new SixteenBitRegister(this.A, this.F);
  public BC = new SixteenBitRegister(this.B, this.C);
  public DE = new SixteenBitRegister(this.D, this.E);

  constructor() {
    this.SP.write(0xfffe);
    this.PC.write(0x1000);
  }
}
