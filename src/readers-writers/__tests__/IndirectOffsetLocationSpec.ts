import { IReadable } from "../../cpu/cpu";
import FakeMemory from "../../test-util/FakeMemory";
import IndirectOffsetLocation from "../IndirectOffsetLocation";

const MEMORY_LOCATION = 0xff02;
const VALUE = 0xbd;
const NEW_VALUE = 0xac;
const OFFSET_FROM = 0xff00;

test("IndirectOffsetLocation", () => {
  const memory = new FakeMemory();
  memory.write(MEMORY_LOCATION, VALUE);

  const offset: IReadable = {
    read() {
      return MEMORY_LOCATION - OFFSET_FROM;
    },
  };

  const indirectOffsetLocation = new IndirectOffsetLocation(memory, offset);
  expect(indirectOffsetLocation.read()).toEqual(VALUE);

  indirectOffsetLocation.write(NEW_VALUE);
  expect(memory.read(MEMORY_LOCATION)).toEqual(NEW_VALUE);
});
