import { sixteenBitAdd } from "../bits";
import { CPU, IInstruction, IReadWritable } from "../cpu/cpu";

export default class Increment16Instruction implements IInstruction {
  constructor(
    readonly cpu: CPU,
    readonly to: IReadWritable,
    readonly cycles: number,
  ) {}

  public execute() {
    const a = this.to.read();
    this.to.write(sixteenBitAdd(a, 1).sum);
  }
}
