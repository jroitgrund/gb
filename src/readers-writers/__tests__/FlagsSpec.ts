import { CPU } from "../../cpu/cpu";

test("Flags", () => {
  const cpu = new CPU();

  expect(cpu.F.getFlags()).toEqual({
    zero: false,
    subtract: false,
    halfCarry: false,
    carry: false,
  });
  expect(cpu.F.read()).toEqual(0b00000000);

  cpu.F.write(0b11000000);
  expect(cpu.F.getFlags()).toEqual({
    zero: true,
    subtract: true,
    halfCarry: false,
    carry: false,
  });
  expect(cpu.F.read()).toEqual(0b11000000);

  cpu.F.setFlags({
    zero: false,
    subtract: false,
    halfCarry: true,
    carry: true,
  });
  expect(cpu.F.getFlags()).toEqual({
    zero: false,
    subtract: false,
    halfCarry: true,
    carry: true,
  });
  expect(cpu.F.read()).toEqual(0b00110000);
});
