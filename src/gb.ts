import { CPU } from "./cpu/cpu";
import { getInstructions } from "./instructions/instructions";
import { Memory } from "./memory";

const memory = new Memory();
const cpu = new CPU();
const instructions = getInstructions(cpu, memory);
console.log(instructions);
