import { IReadable, IReadWritable } from "../cpu/cpu";

export default class ReadAndDecrement implements IReadable {
  constructor(readonly readWritable: IReadWritable) {}

  public read() {
    const value = this.readWritable.read();
    this.readWritable.write(value - 1);
    return value;
  }
}
