import { CPU, IInstruction, IWritable } from "../cpu/cpu";
import { IMemory } from "../memory";

export default class PopInstruction implements IInstruction {
  constructor(
    readonly cpu: CPU,
    readonly memory: IMemory,
    readonly to: IWritable,
    readonly cycles: number,
  ) {}

  public execute() {
    const sp = this.cpu.SP.read();
    const data = this.memory.read16(sp);
    this.to.write(data);
    this.cpu.SP.write(sp + 2);
  }
}
