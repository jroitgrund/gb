import { CPU } from "../../cpu/cpu";
import FakeMemory from "../../test-util/FakeMemory";
import EightBitLiteral from "../EightBitLiteral";

const MEMORY_LOCATION = 500;
const VALUE = 0xbd;

test("EightBitLiteral", () => {
  const cpu = new CPU();
  cpu.PC.write(MEMORY_LOCATION);

  const memory = new FakeMemory();
  memory.write(MEMORY_LOCATION, VALUE);

  const eightBitLiteral = new EightBitLiteral(cpu, memory);
  expect(eightBitLiteral.read()).toEqual(VALUE);
  expect(cpu.PC.read()).toEqual(MEMORY_LOCATION + 1);
});
