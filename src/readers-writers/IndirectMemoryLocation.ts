import { IReadable, IReadWritable } from "../cpu/cpu";
import { IMemory } from "../memory";

export default class IndirectMemoryLocation implements IReadWritable {
  constructor(readonly memory: IMemory, readonly location: IReadable) {}

  public read() {
    return this.memory.read(this.location.read());
  }

  public write(data: number) {
    return this.memory.write(this.location.read(), data);
  }
}
