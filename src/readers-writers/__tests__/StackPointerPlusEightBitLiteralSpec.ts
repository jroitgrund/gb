import { CPU } from "../../cpu/cpu";
import FakeMemory from "../../test-util/FakeMemory";
import EightBitLiteral from "../EightBitLiteral";
import StackPointerPlusEightBitLiteral from "../StackPointerPlusEightBitLiteral";

const PC = 0x10;
const LITERAL = 0b11111100;
const SP = 0xf0;
const VALUE = SP - (~LITERAL & 0xff) - 1;

test("StackPointerPlusEightBitLiteral", () => {
  const cpu = new CPU();
  cpu.PC.write(PC);
  cpu.SP.write(SP);

  const memory = new FakeMemory();
  memory.write(PC, LITERAL);

  const stackPointerPlusEightBitLiteral = new StackPointerPlusEightBitLiteral(
    cpu,
    new EightBitLiteral(cpu, memory),
  );

  expect(stackPointerPlusEightBitLiteral.read()).toEqual(VALUE);
});
