import { IReadWritable } from "../cpu/cpu";

export default class Register implements IReadWritable {
  private value: number = 0;
  public read() {
    return this.value;
  }
  public write(data: number) {
    this.value = data;
  }
}
