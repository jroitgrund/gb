import { CPU, IReadable } from "../cpu/cpu";
import { IMemory } from "../memory";

export default class EightBitLiteral implements IReadable {
  constructor(readonly cpu: CPU, readonly memory: IMemory) {}
  public read() {
    const pc = this.cpu.PC.read();
    this.cpu.PC.write(pc + 1);
    return this.memory.read(pc);
  }
}
