import { CPU, IInstruction, IReadable, IReadWritable } from "../cpu/cpu";

export default class AndInstruction implements IInstruction {
  constructor(
    readonly cpu: CPU,
    readonly to: IReadWritable,
    readonly from: IReadable,
    readonly cycles: number,
  ) {}

  public execute() {
    const a = this.to.read();
    const b = this.from.read();

    const or = a | b;
    const zero = or === 0;

    this.to.write(or);
    this.cpu.F.setFlags({
      carry: false,
      halfCarry: false,
      subtract: false,
      zero,
    });
  }
}
