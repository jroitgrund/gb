import { CPU, IInstruction, IReadable, IReadWritable } from "../cpu/cpu";

export default class XorInstruction implements IInstruction {
  constructor(
    readonly cpu: CPU,
    readonly to: IReadWritable,
    readonly from: IReadable,
    readonly cycles: number,
  ) {}

  public execute() {
    const a = this.to.read();
    const b = this.from.read();

    const xor = a ^ b;
    const zero = xor === 0;

    this.to.write(xor);
    this.cpu.F.setFlags({
      carry: false,
      halfCarry: true,
      subtract: true,
      zero,
    });
  }
}
