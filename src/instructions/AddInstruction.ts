import { eightBitAdd } from "../bits";
import { CPU, IInstruction, IReadable, IReadWritable } from "../cpu/cpu";

export default class AddInstruction implements IInstruction {
  constructor(
    readonly cpu: CPU,
    readonly to: IReadWritable,
    readonly from: IReadable,
    readonly cycles: number,
    readonly carrySupplier: (cpu: CPU) => number = () => 0,
  ) {}

  public execute() {
    const a = this.to.read();
    const b = this.from.read();

    const { sum, carry, halfCarry, zero } = eightBitAdd(
      a,
      b,
      this.carrySupplier(this.cpu),
    );

    this.to.write(sum);
    this.cpu.F.setFlags({ carry, halfCarry, zero, subtract: false });
  }
}

export function withCarry(cpu: CPU) {
  return cpu.F.getFlags().carry ? 1 : 0;
}
