import { getLSB, getMSB, getTwoBytes } from "../bits";
import { IReadWritable } from "../cpu/cpu";

export default class SixteenBitRegister implements IReadWritable {
  constructor(readonly MSB: IReadWritable, readonly LSB: IReadWritable) {}

  public read() {
    return getTwoBytes(this.LSB.read(), this.MSB.read());
  }

  public write(data: number) {
    this.MSB.write(getMSB(data));
    this.LSB.write(getLSB(data));
  }
}
