import { IReadable } from "../../cpu/cpu";
import FakeMemory from "../../test-util/FakeMemory";
import IndirectMemoryLocation from "../IndirectMemoryLocation";

const MEMORY_LOCATION = 500;
const VALUE = 0xbd;
const NEW_VALUE = 0xab;

test("IndirectMemoryLocation", () => {
  const memory = new FakeMemory();
  memory.write(MEMORY_LOCATION, VALUE);

  const location: IReadable = {
    read() {
      return MEMORY_LOCATION;
    },
  };

  const indirectMemoryLocation = new IndirectMemoryLocation(memory, location);
  expect(indirectMemoryLocation.read()).toEqual(VALUE);

  indirectMemoryLocation.write(NEW_VALUE);
  expect(memory.read(MEMORY_LOCATION)).toEqual(NEW_VALUE);
});
